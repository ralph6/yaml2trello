# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name="yaml2trello",
    version="1.0.0",
    description="yaml2trello exports a yaml board to trello",
    license="MIT",
    author="Samuel Maftoul",
    packages=find_packages(),
    install_requires=[
        'PyYAML==5.4.1',
        'requests==2.25.1',
        'jsonschema==3.2.0',
    ],
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.9",
    ],
    scripts=['yaml2trello', 'trello2yaml'],
)
