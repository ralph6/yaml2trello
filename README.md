Installation
============

```
$ pip install git+https://gitlab.com/padok-team/products/experiments/yaml2trello
```

Usage
=====

```
$ # let's export an already existing board (to stdout)
$ # https://trello.com/b/EbCIZb4T/simple-project-board
$ trello2yaml -h
usage: trello2yaml [-h] --board-id BOARD_ID --api-key API_KEY --api-token API_TOKEN

trello2yaml exports a trello board to yaml on stdout

optional arguments:
  -h, --help            show this help message and exit
  --board-id BOARD_ID   Board id (from trello's URL)
  --api-key API_KEY
  --api-token API_TOKEN

$ # https://trello.com/b/EbCIZb4T/simple-project-board
$ trello2yaml --board-id EbCIZb4T --api-key $TRELLO_API_KEY --api-token $TRELLO_API_TOKEN
BRAINSTORM 🤔:
  cards:
    ✋🏿 Add what you'd like to work on below:
      checklists: {}
      desc: ''
TODO 📚:
  cards:
    ✋🏿 Move anything 'ready' here:
      checklists: {}
      desc: ''
DOING ⚙️:
  cards:
    ✋🏿 Move anything that is actually started here:
      checklists: {}
      desc: ''
DONE! 🙌🏽:
  cards:
    ✋🏿 Move anything from doing to done here:
      checklists: {}
      desc: ''
    Inspiration for a Card 📝:
      checklists:
        What needs to be done:
        - Create template card example
        - Create lists (Brainstorm, todo, doing, done)
        - Turn board into template
        - Share with the community
      desc: ''

$ # same command but redirect the board to a file
$ trello2yaml --board-id EbCIZb4T --api-key $TRELLO_API_KEY --api-token $TRELLO_API_TOKEN > board.yaml

$ # import the board
$ yaml2trello -h
usage: yaml2trello [-h] --api-key API_KEY --api-token API_TOKEN --name NAME [-s SHARE_WITH [SHARE_WITH ...]]

yaml2trello imports yaml boards from stdin into trello

optional arguments:
  -h, --help            show this help message and exit
  --api-key API_KEY
  --api-token API_TOKEN
  --name NAME           name of created board
  -s SHARE_WITH [SHARE_WITH ...], --share-with SHARE_WITH [SHARE_WITH ...]
                        email of user to share the boards with

$ cat board.yaml | yaml2trello --name "Simple project board"  --api-key $TRELLO_API_KEY --api-token $TRELLO_API_TOKEN
Starting import of a board ...
Your new board is accessible at https://trello.com/b/XXXXXXXXXXXXXXXXXXXXXXXX

$
```
